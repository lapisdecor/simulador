use std::thread;
use std::time::Duration;

struct Person {
    name: String,
}

impl Person {
    fn talk(&self) -> () {
        println!("{} is talking!", self.name);
        thread::sleep(Duration::from_secs(10));
        println!("{} stoped talking!", self.name);
    }

    fn walk(&self) -> () {
        println!("{} started walking", self.name);
        thread::sleep(Duration::from_secs(5));
        println!("{} stoped walking", self.name);
    }
}

fn main() {
    // Create the sims
    let person1 = Person {
        name: String::from("Luis"),
    };

    let person2 = Person {
        name: String::from("Ana"),
    };

    thread::scope(|s| {
        s.spawn(|| person2.talk());
        s.spawn(|| person1.talk());
        s.spawn(|| person2.walk());
        s.spawn(|| person1.walk());
        s.spawn(|| person1.talk());
        s.spawn(|| person2.walk());
    });
}

